import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { ConfigProvider } from 'antd';
import Root from './routes';
import './index.scss';
import * as serviceWorker from './serviceWorker';
import he_IL from 'antd/es/locale/he_IL';
import 'moment/locale/he'
import moment from 'moment'

moment.locale('he')
class App extends Component {
  componentDidMount() {}

  render() {
    return (
      <ConfigProvider direction={'rtl'} locale={he_IL}>
        <div style={{ width: '100%', height: '100%' }}>
        <Root />
        </div>
    
      </ConfigProvider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { AuthContext } from './context';
import { Spin } from 'antd';

const PrivateRoute = ({ component: Component, ...otherProps }) => {
  const { isAuthenticated,isLoading } = useContext(AuthContext);
  return (
    <Route
      {...otherProps}
      render={(props) =>
        !isLoading?
        isAuthenticated ? <Component {...props} /> : <Redirect to={'/login'} />:<Spin/>
      }
    />
  );
};

export default PrivateRoute;

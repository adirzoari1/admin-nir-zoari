import React, { Component } from 'react';
import { Switch, BrowserRouter, Route, Redirect } from 'react-router-dom';
import { Login, App } from '../screens';
import PrivateRoute from './privateRoute'
function Routes() {
  return (
    <BrowserRouter >
      <Switch>
        <Route exact path='/login' component={Login} />
        <PrivateRoute exact path='/app/*' component={App} />
        <PrivateRoute exact path='/app' component={App} />
        <Route exact path='/' render={() => <Redirect to='/app' />} />
      </Switch>
    </BrowserRouter>
  );
}


export default Routes
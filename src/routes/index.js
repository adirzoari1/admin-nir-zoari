import React, { useState, useEffect, useCallback } from "react";
import Routes from "./routes";
import _ from 'lodash'
import { AuthContext } from "./context";


function App() {
  const [token, setToken] = useState(null);
  const [user,setUser] = useState({})
  const [isLoading,setLoading] = useState(true)

  const setUserData = useCallback((data ) => {
    setToken(data.token);
    setUser(data.user)
    localStorage.setItem(
      "user",
      JSON.stringify({
          token:data.token,
          data:data.user
      })
    );
  }, []);

  const logout = useCallback(() => {
    setToken(null);
    localStorage.removeItem("user");
  }, []);

  useEffect(() => {
    setLoading(true)
    const userData = JSON.parse(localStorage.getItem("user"));
    if(userData && userData.token){
      setToken(_.get(userData,'token',null))
      setUser(_.get(userData,'data',{}))
    }
    setLoading(false)

    
  }, []);





  return (
    <AuthContext.Provider
      value={{
        isAuthenticated: !!token,
        token: token,
        setUserData: setUserData,
        logout: logout,
        user:user,
        isLoading:isLoading
      }}
    >
      
      <Routes/>
     
    </AuthContext.Provider>
  );
}
export default App
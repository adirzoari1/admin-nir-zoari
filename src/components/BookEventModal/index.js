import React, {  useEffect, useState } from 'react';
import { Form, Input, Select, Modal, notification } from 'antd';
import moment from 'moment';
import _ from 'lodash';
import {
  tailFormItemLayout,
  formItemLayout,
} from '../../utils/formLayoutUtils';
import LoadingIndicator from '../LoadingIndicator';
import Api from '../../utils/api';
const { Option } = Select;



const BABRER_ID = 1; // TODO: to change it to get from auth
const BOOK_EVENT_OPTIONS = [
  {id:'CANCEL_BOOK_APPOINTMENT',label:'ביטול תור'},
  {id:'SUBMIT_BOOK_APPOINTMENT',label: 'אישור הגעה'},
  {id:"CALL_CLIENT", label: 'התקשר ללקוח'}
]
function BookEventModal({ visible, onSave, onCloseModal,bookEventData,getBookingsByDate }) {
  const [form] = Form.useForm();
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    form.resetFields();
  }, [visible, form]);

  useEffect(() => {
  }, []);



  async function onFinish() {
    try {
      let values = await form.validateFields();
     
      switch(values.bookEventOption){
        case 'CANCEL_BOOK_APPOINTMENT':{
          cancelBookAppointment()
          break;
        }

        case 'SUBMIT_BOOK_APPOINTMENT':{
          submitBookAppointment()
          break;
        }
        case 'CALL_CLIENT':{
          callClient()
          break;
        }
        default:{

        }
        
      }
    } catch (e) {
    }
  }

  async function cancelBookAppointment(){
    try{
      setLoading(true)
      await Api.cancelAppointment(bookEventData.data.appointment_id)
      await getBookingsByDate(bookEventData.start)
      setLoading(false)
      notification.success({
        message: 'הצלחה',
        description:'התור בוטל בהצלחה'
      })
      onCloseModal();
      
    }catch(err){
      notification.error({
        message: 'שגיאה',
        description: err.message,
      })

    }
  }

  async function submitBookAppointment(){
    try{
      setLoading(true)
      const data = {
        appointment_id:bookEventData.data.appointment_id
      }
      await Api.updateUserArrive(data)
      setLoading(false)
      notification.success({
        message: 'הצלחה',
        description:'התור אושר בהצלחה'
      })
      onCloseModal();
      
    }catch(err){
      notification.error({
        message: 'שגיאה',
        description: err.message,
      })

    }
  }

  function callClient(){
    const callPhone = window.open(`tel:${bookEventData.data.phone}`,"_blank");
    callPhone.close();
    onCloseModal()
  }
 
  const onReset = () => {
    form.resetFields();
    onCloseModal();
  };

  return (
    <Modal
      visible={visible}
      title={'בחר מה תרצה לעשות'}
      onCancel={onReset}
      onOk={onFinish}
      okText={'שמור'}
      cancelText={'ביטול'}
      destroyOnClose={true}
      width={800}
      forceRender={true}
    >
      <LoadingIndicator isLoading={isLoading}>
        <Form
          {...formItemLayout}
          form={form}
          name='book_event_form'
          scrollToFirstError
          initialValues={{
            bookEventOption:BOOK_EVENT_OPTIONS[0].id
          }}
        >
          

          <Form.Item
            name='bookEventOption'
            label='בחר אפשרות'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
           
          >
            <Select>
              {BOOK_EVENT_OPTIONS &&
                BOOK_EVENT_OPTIONS.map((value, i) => {
                  return (
                    <Option key={value.id} value={value.id}>
                      {value.label}
                    </Option>
                  );
                })}
            </Select>
          </Form.Item>

  
        </Form>
      </LoadingIndicator>
    </Modal>
  );
}

export default BookEventModal;

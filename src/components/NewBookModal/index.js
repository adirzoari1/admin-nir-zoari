import React, {  useEffect, useState } from 'react';
import { Form, Input, Select, Modal, Divider, DatePicker, Switch,notification } from 'antd';
import moment from 'moment';
import _ from 'lodash';
import {
  tailFormItemLayout,
  formItemLayout,
} from '../../utils/formLayoutUtils';
import LoadingIndicator from '../LoadingIndicator';
import Api from '../../utils/api';
const { Option } = Select;

const DEFAULT_QUERY = {
  LIMIT: 10000,
  OFFSET: 0,
};

const BABRER_ID = 1; // TODO: to change it to get from auth

function NewBookModal({ visible, onSave, onCloseModal }) {
  const [bookData, setBookData] = useState({
    users: [],
    services: [],
    barberServices: [],
    servicesProviders: [],
  });
  const [form] = Form.useForm();
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    form.resetFields();
  }, [visible, form]);

  useEffect(() => {
    getData();
  }, []);

  async function getData(date) {
    setLoading(true);

    const promises = [
      Api.getAllUsers(DEFAULT_QUERY.LIMIT, DEFAULT_QUERY.OFFSET),
      Api.getServicesBybarber(BABRER_ID),
      Api.getAllBarbersServices(),
    ];

    try {
      let [users, services, barberServices] = await Promise.all(promises);
      users = _.get(users, 'data.users', []);
      services = _.get(services, 'data.services', []);
      barberServices = _.get(barberServices, 'data.services', []);

      setBookData((prevState) => ({
        ...prevState,
        users,
        services,
        barberServices,
      }));

      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }

  async function getAvailableScheduleOfBarber(date) {
    setLoading(true)
    const query = {
      service_id: form.getFieldValue('service_id'),
      barber_id: form.getFieldValue('service_provider'),
      currentTime: moment(new Date()).format('HH:mm'),
      current_date: moment(date).format('YYYY-MM-DD'),
    };

    try {
      let slots = await Api.getAvailableScheduleOfBarber(query);
      slots = _.get(slots, 'data.schedule[0].slots', []);
      setBookData((prevState) => ({
        ...prevState,
        slots,
      }));
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }

  async function onFinish() {
    try {
      setLoading(true);

      const values = await form.validateFields();
      let userId = values.user_id;
      let selectedUser;
      if (!values.user_id) {
        const newUserData = {
          first_name: values.first_name,
          last_name: values.last_name,
          phone: values.phone,
        };
        const user = await Api.createNewUser(newUserData);
        userId = _.get(user, 'data.user[0]');
        selectedUser = { ...newUserData };
      } else {
        selectedUser = _.filter(
          bookData.users,
          (s) => +s.user_id == +values.user_id
        )[0];
      }

      const selectedService = _.filter(
        bookData.services,
        (s) => s.service_id == values.service_id
      )[0];
      const selectedServiceProvider = _.filter(
        bookData.servicesProviders,
        (s) => s.barber_id == values.service_provider
      )[0];
      const selectedSlot = _.filter(
        bookData.slots,
        (s) => s.slot == +values.time
      )[0];

      const data = {
        user_id: userId,
        barber_id: values.service_provider,
        service_id: values.service_id,
        date: moment(values.date).format('YYYY-MM-DD'),
        time: selectedSlot.time,
        slot: selectedSlot.slot,
        current_date: moment().format('YYYY-MM-DD'),
        current_time: moment().format('HH:mm'),
        service_type: selectedService.service_type,
        first_name: selectedUser.first_name,
        barber_first_name: selectedServiceProvider.first_name,
      };

      await Api.addAppointment(data);
      setLoading(false);

      notification.success({
        message: 'הצלחה',
        description:'התור נוצר בהצלחה'
      })
      onCloseModal();
    } catch (e) {
    }
  }

  const onReset = () => {
    form.resetFields();
    onCloseModal();
  };

  return (
    <Modal
      visible={visible}
      title={'יצירת תור חדש'}
      onCancel={onReset}
      onOk={onFinish}
      okText={'שמור'}
      cancelText={'ביטול'}
      destroyOnClose={true}
      width={800}
      forceRender={true}
    >
      <LoadingIndicator isLoading={isLoading}>
        <Form
          {...formItemLayout}
          form={form}
          name='new_book_form'
          scrollToFirstError
        >
          <Divider>בחירת לקוח</Divider>
          <Form.Item
            name='createNewCustomer'
            label='יצירת לקוח חדש'
            valuePropName='checked'
          >
            <Switch />
          </Form.Item>

          <Form.Item
            noStyle
            shouldUpdate={(prevValues, currentValues) =>
              prevValues.createNewCustomer !== currentValues.createNewCustomer
            }
          >
            {({ getFieldValue }) =>
              getFieldValue('createNewCustomer') ? (
                <>
                  <Form.Item
                    name='first_name'
                    label='שם פרטי'
                    rules={[
                      {
                        required: true,
                        message: 'שדה חובה',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item
                    name='last_name'
                    label='שם משפחה'
                    rules={[
                      {
                        required: true,
                        message: 'שדה חובה',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item
                    name='phone'
                    label='טלפון'
                    rules={[
                      {
                        validator: (rule, value, callback) => {
                          if (value && value.length != 10) {
                            callback('מספר טלפון לא תקין');
                          }
                          callback();
                        },
                      
                      },
                      {
                        required:true,
                        message:'שדה חובה'
                      }
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </>
              ) : (
                <Form.Item
                  name='user_id'
                  label='בחר לקוח קיים'
                  rules={[
                    {
                      required: true,
                      message: 'שדה חובה',
                    },
                  ]}
                >
                  <Select>
                    {bookData.users &&
                      bookData.users.map((value, i) => {
                        return (
                          <Option key={value.user_id} value={value.user_id}>
                            {value.first_name} {value.last_name}
                          </Option>
                        );
                      })}
                  </Select>
                </Form.Item>
              )
            }
          </Form.Item>

          <Divider>בחירת שירות</Divider>
          <Form.Item
            name='service_id'
            label='בחירת שירות'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
            getValueFromEvent={(id) => {
              setBookData((prevState) => ({
                ...prevState,
                servicesProviders: _.filter(
                  bookData.barberServices,
                  (s) => s.service_id == id
                )[0].services_providers,
              }));

              return id;
            }}
          >
            <Select>
              {bookData.barberServices &&
                bookData.barberServices.map((value, i) => {
                  return (
                    <Option key={value.service_id} value={value.service_id}>
                      {value.service_type}
                    </Option>
                  );
                })}
            </Select>
          </Form.Item>

          <Form.Item
            noStyle
            shouldUpdate={(prevValues, currentValues) =>
              prevValues.service_id !== currentValues.service_id
            }
          >
            {({ getFieldValue }) =>
              getFieldValue('service_id') ? (
                <Form.Item
                  name='service_provider'
                  label='בחירת נותן שירות'
                  rules={[
                    {
                      required: true,
                      message: 'שדה חובה',
                    },
                  ]}
                >
                  <Select>
                    {bookData.servicesProviders &&
                      bookData.servicesProviders.map((value, i) => {
                        return (
                          <Option key={value.barber_id} value={value.barber_id}>
                            {value.first_name} {value.last_name}
                          </Option>
                        );
                      })}
                  </Select>
                </Form.Item>
              ) : null
            }
          </Form.Item>

          <Form.Item
            noStyle
            shouldUpdate={(prevValues, currentValues) =>
              prevValues.service_provider !== currentValues.service_provider
            }
          >
            {({ getFieldValue }) =>
              getFieldValue('service_provider') ? (
                <Form.Item
                  name='date'
                  label='תאריך'
                  rules={[
                    {
                      required: true,
                      message: 'שדה חובה',
                    },
                  ]}
                  getValueFromEvent={(date) => {
                    getAvailableScheduleOfBarber(date);
                    return date;
                  }}
                >
                  <DatePicker
                    disabledDate={(current) =>
                      current && current <= moment().startOf('day')
                    }
                  />
                </Form.Item>
              ) : null
            }
          </Form.Item>

          {/*TODO:  to change!!! to make slots */}
          <Form.Item
            noStyle
            shouldUpdate={(prevValues, currentValues) =>
              prevValues.date !== currentValues.date
            }
          >
            {({ getFieldValue }) =>
              getFieldValue('date') ? (
                <Form.Item
                  name='time'
                  label='שעה'
                  rules={[
                    {
                      required: true,
                      message: 'שדה חובה',
                    },
                  ]}
                >
                  <Select>
                    {bookData.slots &&
                      bookData.slots.map((value, i) => {
                        return (
                          <Option
                            key={value.slot}
                            value={value.slot.toString()}
                          >
                            {value.time}
                          </Option>
                        );
                      })}
                  </Select>
                </Form.Item>
              ) : null
            }
          </Form.Item>
        </Form>
      </LoadingIndicator>
    </Modal>
  );
}

export default NewBookModal;

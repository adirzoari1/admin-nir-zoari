import React, { useMemo, useEffect, useState } from 'react';
import {
  Form,
  Input,
  Select,
  Modal,
  DatePicker,
  Switch,
  notification,
} from 'antd';

import _ from 'lodash';
import moment from 'moment';

import {
  tailFormItemLayout,
  formItemLayout,
} from '../../utils/formLayoutUtils';
import Api from '../../utils/api';
import LoadingIndicator from '../LoadingIndicator';
const { Option } = Select;
const { RangePicker } = DatePicker;

export const NOTIFICATIONS_TYPES = [
  { value: 'app_notification', text: 'הודעה באפליקציה' },
  { value: 'push_notification', text: 'הודעת פוש נוטיפיקישין' },
];

function NewNotificationModal({ visible, onSubmit, onCloseModal, record }) {
  const [form] = Form.useForm();
  const [isLoading, setLoading] = useState(false);
  const BARBER_ID = 1; // TODO: to change it to take from auth
  useEffect(() => {}, []);
  useEffect(() => {
    form.resetFields();
  }, [visible, form]);

  async function onFinish() {
    try {
      setLoading(true);
      const values = await form.validateFields();
      console.log('values are', values);
      const data = {
        description: _.get(values, 'description', ''),
        barber_id: BARBER_ID,
        title: _.get(values, 'title', ''),
        image: _.get(values, 'image', ''),
        type: _.get(values, 'type', ''),
        service_id: _.get(values, 'service_id', 1),
        start_date: moment(_.get(values.dates, '[0]', moment())).format(
          'MM/DD/YYYY'
        ),
        end_date: moment(_.get(values.dates, '[1]', moment())).format(
          'MM/DD/YYYY'
        ),
      };

      switch (values.type) {
        case 'app_notification': {
          await Api.addNotification(data);
          break;
        }

        case 'push_notification': {
          await Api.sendGeneralPushNotification(data);
          break;
        }
      }
      setLoading(false);
      notification.success({
        message: 'הצלחה',
        description: 'ההודעה נשלחה בהצלחה',
      });
      onSubmit()
    } catch (err) {
      setLoading(false);

      notification.error({
        message: 'שגיאה',
        description: err.message,
      });
    }
  }
  const onReset = () => {
    form.resetFields();
    onCloseModal();
  };

  return (
    <Modal
      visible={visible}
      title={'הוספת הודעה'}
      onCancel={onReset}
      onOk={onFinish}
      okText={'שמור'}
      cancelText={'ביטול'}
      destroyOnClose={true}
      width={800}
      forceRender={true}
    >
      <LoadingIndicator isLoading={isLoading}>
        <Form
          {...formItemLayout}
          form={form}
          name='form_new_notification'
          initialValues={{
            type: 'app_notification',
          }}
          scrollToFirstError
        >
          <Form.Item
            name='type'
            label='סוג הודעה'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <Select>
              {_.map(NOTIFICATIONS_TYPES, (notification, i) => {
                return (
                  <Option key={i} value={notification.value}>
                    {notification.text}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item
            noStyle
            shouldUpdate={(prevValues, currentValues) =>
              prevValues.type !== currentValues.type
            }
          >
            {({ getFieldValue }) =>
              getFieldValue('type') === NOTIFICATIONS_TYPES[1].value ? (
                <Form.Item
                  name='title'
                  label='כותרת הודעה'
                  rules={[
                    {
                      required: true,
                      message: 'שדה חובה',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              ) : null
            }
          </Form.Item>

          <Form.Item
            name='description'
            label='תוכן הודעה'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            noStyle
            shouldUpdate={(prevValues, currentValues) =>
              prevValues.type !== currentValues.type
            }
          >
            {({ getFieldValue }) =>
              getFieldValue('type') === NOTIFICATIONS_TYPES[0].value ? (
                <Form.Item
                  name='dates'
                  label='תאריכים'
                  rules={[
                    {
                      required: true,
                      message: 'שדה חובה',
                    },
                  ]}
                >
                  <RangePicker
                    disabledDate={(current) =>
                      current && current <= moment().startOf('day')
                    }
                    format='DD/MM/YYYY'
                  />
                </Form.Item>
              ) : null
            }
          </Form.Item>
        </Form>
      </LoadingIndicator>
    </Modal>
  );
}

export default NewNotificationModal;

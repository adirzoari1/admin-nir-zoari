import React, { useMemo, useEffect, useState } from 'react';
import { Slider, Checkbox,Typography } from 'antd';
import { HourSliderStyle,DayText,TimeText } from './style';
import moment from 'moment';
import 'moment/locale/he';
import locale from 'antd/lib/locale-provider/he_IL';

moment.locale('he');
const { Text,Title } = Typography;
function formatter(value) {
  let time = moment()
    .clone()
    .set({ hour: 0, minutes: 0 })

    .add(value, 'hours')
    .format('HH:mm');

  return `${time}`;
}

function HourSlider({index, active, start_shift, end_shift, day, onChecked,onChange }) {
  return (
    <HourSliderStyle align="middle" justify-content="space-between">
   
      <DayText>
     {day}
     </DayText>
      <TimeText >
        {moment()
          .clone()
          .set({ hour: 0, minutes: 0 })

          .add(end_shift, 'hours')
          .format('HH:mm')}
      </TimeText>
      <Slider
        range
        min={0.0}
        max={24.0}
        step={0.5}
        defaultValue={[start_shift, end_shift]}
        value={[start_shift, end_shift]}
        tipFormatter={formatter}
        disabled={!active}
        onChange={(e)=>{
          onChange({checked:true,startShift:e[0],endShift:e[1],scheduleId:index,e})
        }}
        
      />
      <TimeText>
        {moment()
          .clone()
          .set({ hour: 0, minutes: 0 })

          .add(start_shift, 'hours')
          .format('HH:mm')}
      </TimeText>
    </HourSliderStyle>
  );
}

export default HourSlider;

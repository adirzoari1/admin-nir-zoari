import styled from "styled-components";
import {Slider,Row,Typography } from 'antd';

const {Text} = Typography

const HourSliderStyle = styled(Row)`
display: flex;
justify-content: center;
align-items: center;
.ant-slider {
  flex: 1;
}
.ant-checkbox-wrapper {
  width: 80px;
}
`

const DayText = styled(Text)`
  width:80px;
 
`

const TimeText = styled.small`
`
export { HourSliderStyle,TimeText,DayText}
import React, { Component } from 'react';
import { Layout, Menu, Icon,Button } from 'antd';
import styled from 'styled-components';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import {
  CalendarOutlined,
  UserOutlined,
  AppstoreOutlined,
  MessageOutlined,
  SettingOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined
} from '@ant-design/icons';
import { StyledSider,Logo } from './style';
// import LogoImage from '../../assets/images/logo.png'
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

class Aside extends Component {


  render() {
    return (
      <StyledSider
        theme='dark'
        trigger={null}
        collapsible 
        collapsed={this.props.collapsed}
        collapsedWidth={0}

        
      >
        <Logo className='dark'></Logo>
          {/* <img src={LogoImage} alt="logo"/> */}
       
        <Menu
          theme='dark'
          mode='inline'
          selectedKeys={[this.props.location.pathname]}
        >
          <Menu.Item key='/app' icon={ <AppstoreOutlined />}>
            <Link to='/app'>
             
              ראשי
            </Link>
          </Menu.Item>
          <Menu.Item key='/app/booking' icon={  <CalendarOutlined />}>
            <Link to='/app/booking'>
            
              תורים
            </Link>
          </Menu.Item>
          <Menu.Item key='/app/customers' icon={ <UserOutlined />}>
            <Link to='/app/customers'>
             
              לקוחות
            </Link>
          </Menu.Item>

          <Menu.Item key='/app/notifications' icon={   <MessageOutlined />}>
            <Link to='/app/notifications'>
           
              הודעות
            </Link>
          </Menu.Item>

          <SubMenu
            key='business_managment'
            icon={<SettingOutlined />}
            title='ניהול עסק'
          >
            <Menu.Item key='/app/business-general-details'>
              <Link to='/app/business-general-details'>פרטי העסק</Link>
            </Menu.Item>
            <Menu.Item key='/app/service-providers'>
              <Link to='/app/service-providers'>
                נותני שירות
              </Link>
            </Menu.Item>
            <Menu.Item key='/app/services'>
              <Link to='/app/services'>שירותים</Link>
            </Menu.Item>
            <Menu.Item key='/app/schedule'>
            <Link to='/app/schedule'> שעות פעילות</Link>
          </Menu.Item>
          <Menu.Item key='/app/breaks'>
            <Link to='/app/breaks'> הפסקות</Link>
          </Menu.Item>
            <Menu.Item key='4'>תאריכים מיוחדים</Menu.Item>
          </SubMenu>
        </Menu>
      </StyledSider>
    );
  }
}

export default withRouter(Aside);



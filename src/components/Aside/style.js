import styled from "styled-components";
import {Layout} from "antd";
const { Sider } = Layout


const StyledSider = styled(Sider)`
 background-color:#000;

 .ant-menu-item .ant-menu-item-only-child .ant-menu-item-selected{
            background-color: #ff3860 !important;
      border-color: #ff3860 !important;
      background-image: -webkit-linear-gradient(309deg, #ff0561 0%, #ff3860 71%, #ff5257 100%) !important;
      background-image: linear-gradient(141deg, #ff0561 0%, #ff3860 71%, #ff5257 100%) !important;
    }
  .ant-menu.ant-menu-dark .ant-menu-item-selected,
    .ant-menu-submenu-popup.ant-menu-dark .ant-menu-item-selected {
      background-color: #ff3860 !important;
      border-color: #ff3860 !important;
      background-image: -webkit-linear-gradient(309deg, #ff0561 0%, #ff3860 71%, #ff5257 100%) !important;
      background-image: linear-gradient(141deg, #ff0561 0%, #ff3860 71%, #ff5257 100%) !important;
    }
  
    .ant-menu-submenu-vertical > .ant-menu-submenu-title .ant-menu-submenu-arrow,
    .ant-menu-submenu-vertical-left > .ant-menu-submenu-title .ant-menu-submenu-arrow,
    .ant-menu-submenu-vertical-right > .ant-menu-submenu-title .ant-menu-submenu-arrow,
    .ant-menu-submenu-inline > .ant-menu-submenu-title .ant-menu-submenu-arrow {
      right: auto;
      left: 16px;
    }
  
    .ant-menu-inline .ant-menu-submenu-title {
      padding-right: 16px;
      padding-left: 34px;
    }
  
    .ant-menu.ant-menu-dark{
      background:#000;
    }
    .ant-menu-dark .ant-menu-inline.ant-menu-sub {
      background: #000;
      li {
        padding-right: 48px;
        padding-left: 0;
      }
     
    }
`

const Logo = styled.div`
  height: 80px;
  display: flex;
  justify-content: center;
  align-items: center;
  img {
    height: 40px;
  }
`;

export {
  StyledSider,
  Logo
}
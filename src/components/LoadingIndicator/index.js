import React from 'react'
import { Spin } from 'antd'
import  get  from 'lodash/get'
// import { useSelector } from 'react-redux';
function LoadingIndicator({children,forComponents=[],isLoading=false}) {
    // const httpState = useSelector(httpStateSelector)
    // const isLoading = forComponents.some(
    //     s => get(httpState, [s, 'count'], 0) > 0
    //   );
      return <Spin spinning={isLoading}>{children}</Spin>;
    
}

export default LoadingIndicator

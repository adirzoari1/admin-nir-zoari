import React, { useMemo, useEffect, useState } from 'react';
import {
  Form,
  Input,
  Select,
  Modal,
  InputNumber,
  Avatar,
  Switch,
  Tooltip,
  notification,
} from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';

import _ from 'lodash';
import {
  tailFormItemLayout,
  formItemLayout,
} from '../../utils/formLayoutUtils';
import Api from '../../utils/api';
import LoadingIndicator from '../LoadingIndicator';
const { Option } = Select;

function UpsertServiceModal({ visible, onCloseModal, record,onSubmit }) {
  const [form] = Form.useForm();
  const [servicesProviders, setServicesProviders] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const editMode = useMemo(() => {
    return record.service_id != null;
  }, [record]);

  useEffect(() => {
    getAllServiceProviders();
  }, []);
  useEffect(() => {
    form.resetFields();
  }, [visible, form]);

  async function getAllServiceProviders() {
    try {
      setLoading(true);
      let res = await Api.getAllBarbers();
      res = _.get(res, 'data.users', []);
      setServicesProviders([...res]);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }
  async function onFinish() {
    try {
      setLoading(true)
      const values = await form.validateFields();
      const data = {
        service_id: _.get(record, 'service_id', null),
        service_type: _.get(values, 'service_type', ''),
        service_description: _.get(values, 'service_description', ''),
        time_slot: _.get(values, 'time_slot', 5),
        gender: _.get(values, 'gender', ''),
        icon: _.get(values, 'icon', 1),
        show: _.get(values, 'show', true),
        price: _.get(values, 'price', 0),
        only_preview: _.get(values, 'only_preview', false),
        message: _.get(values, 'message', ''),
        barbers: _.get(values, 'services_providers', []),
        display_fixed_slot:_.get(values, 'display_fixed_slot', 5),
      };

      if(data.service_id){
        await Api.updateService(data)
        notification.success({
          message: 'הצלחה',
          description:'השירות עודכן בהצלחה'
        })
      }else{
        await Api.addService(data)
        notification.success({
          message: 'הצלחה',
          description:'השירות נוצר בהצלחה'
        })
      }


      setLoading(false)
   
      onSubmit();
    } catch (err) {
      setLoading(false)

      notification.error({
        message: 'שגיאה',
        description: err.message,
      })
    }
  }
  const onReset = () => {
    form.resetFields();
    onCloseModal();
  };

  return (
    <Modal
      visible={visible}
      title={editMode ? 'עריכת שירות' : 'הוספת שירות'}
      onCancel={onReset}
      onOk={onFinish}
      okText={'שמור'}
      cancelText={'ביטול'}
      destroyOnClose={true}
      width={800}
      forceRender={true}
    >
      <LoadingIndicator isLoading={isLoading}>
        <Form
          {...formItemLayout}
          form={form}
          name='upsert_service_modal'
          initialValues={{
            service_type: _.get(record, 'service_type', ''),
            service_description: _.get(record, 'service_description', ''),
            price: _.get(record, 'price', ''),
            time_slot: _.get(record, 'time_slot', 0),
            only_preview: _.get(record, 'only_preview', false),
            show: _.get(record, 'show', true),
            message: _.get(record, 'message', ''),
            icon: +(_.get(record, "icon", 1)),
            services_providers: _.get(record, 'services_providers', []).map(
              (s) => s.barber_id
            ),
            display_fixed_slot:_.get(record,'display_fixed_slot',5)
          }}
          scrollToFirstError
        >
          <Form.Item
            name='service_type'
            label='סוג השירות'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item name='show' label='סטטוס שירות' valuePropName='checked'>
            <Switch />
          </Form.Item>
          <Form.Item
            name='only_preview'
            label={
              <span>
                להצגה בלבד&nbsp;
                <Tooltip title='השירות יהיה מוצג אך ללא בחירה בו'>
                  <QuestionCircleOutlined />
                </Tooltip>
              </span>
            }
            valuePropName='checked'
          >
            <Switch />
          </Form.Item>

          <Form.Item
            noStyle
            shouldUpdate={(prevValues, currentValues) =>
              prevValues.only_preview !== currentValues.only_preview
            }
          >
            {({ getFieldValue }) =>
              getFieldValue('only_preview') ? (
                <Form.Item
                  name='message'
                  label='הודעת הצגה'
                  rules={[
                    {
                      required: true,
                      message: 'שדה חובה',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              ) : null
            }
          </Form.Item>

          <Form.Item name='service_description' label='תיאור השירות'>
            <Input />
          </Form.Item>

          <Form.Item
            name='price'
            label='מחיר'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name='icon'
            label='תמונה'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <Select>
              {_.range(1, 30).map((i) => {
                let img_src = require(`../../assets/images/services/icon_${i}.png`);
                let value = i;
                return (
                  <Option value={value}>
                    <Avatar src={img_src} shape='square' size='small' />
                  </Option>
                );
              })}
            </Select>
          </Form.Item>

          <Form.Item
            name='time_slot'
            label='מרווח זמן'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
              {
                validator: (_, value) =>
                  value % 5 === 0
                    ? Promise.resolve()
                    : Promise.reject('מרווח הזמן צריך להיות בקפיצות של 5'),
              },
            ]}
          >
            <InputNumber min={0} step={5} />
          </Form.Item>

          <Form.Item
            name='display_fixed_slot'
            label='תצוגת זמן'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
              {
                validator: (_, value) =>
                  value % 5 === 0
                    ? Promise.resolve()
                    : Promise.reject('מרווח הזמן צריך להיות בקפיצות של 5'),
              },
            ]}
          >
            <InputNumber min={0} step={5} />
          </Form.Item>

          <Form.Item
            name='services_providers'
            label='נותני שירות'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
                type: 'array',
              },
            ]}
          >
            <Select mode='multiple'>
              {servicesProviders &&
                servicesProviders.map((value, i) => {
                  return (
                    <Option value={value.barber_id}>
                      {value.first_name} {value.last_name}
                    </Option>
                  );
                })}
            </Select>
          </Form.Item>
        </Form>
      </LoadingIndicator>
    </Modal>
  );
}

export default UpsertServiceModal;

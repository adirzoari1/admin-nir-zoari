import React, { useMemo, useEffect, useState } from 'react';
import { Layout,PageHeader } from 'antd';
import { Container, ContainerBox } from './style';
const { Content } = Layout;

function PageContent({ children,headerTitle }) {
  return (
    <Container>

      <ContainerBox>
      {headerTitle && <PageHeader title={headerTitle}/>}

       {children}
      </ContainerBox>
    </Container>
  );
}

export default PageContent;

import styled from "styled-components";
import {Slider,Row,Typography } from 'antd';

const {Text} = Typography


const Container = styled.div`
  margin: 0 auto;
  max-width: 1344px;
 
`

const ContainerBox = styled.div`
    box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);
    background-color: #fff;
    padding:20px;
    overflow-x:auto;
`
export { Container,ContainerBox}
import React from "react";

import { Dropdown, Button, Menu,Switch, Avatar} from "antd";
import { BarsOutlined, DownOutlined } from "@ant-design/icons";


function getColumns(onSelectMenu,onShowUpsertModal,onChangeServiceStatus) {
  const columns = [
    {
      title: "מזהה שירות",
      dataIndex: "service_id",
      width:'10%'
    },
    {
      title: "סוג השירות",
      dataIndex: "service_type",
    },
   
    {
      title: "מחיר",
      dataIndex: "price",
      render: (price) => `${price} ₪`

    },
    {
      title: "מרווח זמן",
      dataIndex: "time_slot",
      render: (time_slot) => `${time_slot} דקות`

    },
    {
      title: "תמונה",
      dataIndex: "icon",
      render:(icon)=><Avatar src={require(`../../assets/images/services/icon_${icon}.png`)} shape="square" size="small"/>,

    },

    {
      title: "סטטוס",
      dataIndex: "show",
      render:(show)=><Switch defaultChecked={show} onChange={onChangeServiceStatus} />,

    },

    {
      key: "operation",
      render: (text, record) => {
        const menu = [
          { key: "edit_service", name: "עדכן" },
        ].map((item) => <Menu.Item key={item.key}>{item.name}</Menu.Item>);
        return (
          <Dropdown trigger={['click']} overlay={<Menu onClick={(e)=>{onSelectMenu(e.key,record)}}>{menu}</Menu>}>
            <Button style={{ border: "none" }}>
              <BarsOutlined style={{ marginRight: 2 }} />
              <DownOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  return columns;
}

export default getColumns
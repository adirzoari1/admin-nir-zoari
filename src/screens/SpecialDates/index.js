import React, { useEffect, useState, useMemo } from "react";
import { Layout, Table, PageHeader,Button } from "antd";
import _ from "lodash";
// import { getServices } from "../../store/services/services.actions";
// import { servicesSelector } from "../../store/services/services.selectors";
import getColumns from "./columns";
import { UpsertServiceModal } from "../../components";
// import { getServicesProviders } from "../../store/servicesProviders/servicesProviders.actions";
import { PlusOutlined } from "@ant-design/icons";

const { Content } = Layout;

function Services() {
  const services = useState([])
  const [upsertModalVisible, setUpsertServiceVisible] = useState(false);
  const [selectedRecord, setSelectedRecord] = useState({});

  // useEffect(() => {
  //   dispatch(getServices());
  //   dispatch(getServicesProviders());
  // }, [dispatch]);

  function onShowUpsertModal(record) {
    setSelectedRecord({ ...record });
    setUpsertServiceVisible(true);
  }

  function onSelectMenu(key, record) {
    switch (key) {
      case "edit_service":
        onShowUpsertModal(record);
        break;

      default:
        break;
    }
  }
  const tableColumns = useMemo(
    () => getColumns(onSelectMenu, onShowUpsertModal),
    [onSelectMenu]
  );

  return (
    <div className="container">
      <PageHeader title="שירותים" className="site-page-header"/>
      <Content>
        <Button  type="primary" icon={<PlusOutlined />} onClick={()=>{onShowUpsertModal({})}} style={{marginBottom:'20px'}}>
          הוספת שירות
        </Button>
        <Table
          dataSource={services}
          pagination={{
            pageSize: 10,

            // showTotal: total => i18n.t`Total ${total} Items`,
          }}
          scroll={{ x: 1200 }}
          columns={tableColumns}
          simple
          rowKey="service_id"
        />
        <UpsertServiceModal
          visible={upsertModalVisible}
          record={selectedRecord}
          onCloseModal={() => {
            setSelectedRecord({});
            setUpsertServiceVisible(false);
          }}
        />
      </Content>
    </div>
  );
}

export default Services;

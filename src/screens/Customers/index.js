import React, { useEffect, useState, useMemo } from 'react';
import { Table, PageHeader, Layout, Row, Col, Input, AutoComplete } from 'antd';
import _ from 'lodash';
import getColumns from './columns';
import { LoadingIndicator, PageContent } from '../../components';
import Api from '../../utils/api';
const { Content } = Layout;
const { Search } = Input;
const { Option } = AutoComplete;

const componentName = 'Customers';
const DEFAULT_QUERY = {
  LIMIT: 10,
  OFFSET: 0,
};
function Customers() {
  const [users, setUsers] = useState([]);
  const [searchUser, setSearchUser] = useState('');
  const [selectedOption, setSelectedOption] = useState('');
  const [tableData, setTableData] = useState([]);
  const [usersOptions, setUserOptions] = useState([]);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    getUsers(DEFAULT_QUERY.LIMIT, DEFAULT_QUERY.OFFSET);
  }, []);

  useEffect(() => {
    if (users && searchUser) {
      const filteredData = _.filter(
        users,
        (user) =>
          user &&
          `${user.first_name} ${user.last_name}`
            .toLowerCase()
            .includes(searchUser.toLowerCase())
      );
      setTableData([...filteredData]);
    }
  }, [searchUser, users]);

  useEffect(() => {
    if (users) {
      setUserOptions(getUserOptions(users));
    }
  }, [users]);

  async function getUsers(limit, offset) {
    try {
      setLoading(true);
      let res = await Api.getAllUsers(limit, offset);
      res = _.get(res, 'data.users', []);
      setUsers([...res]);
      setTableData([...res])
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }
 

  async function onChangeFavouriteCustomer(userId, favourite) {
    try {
      setLoading(true);
      const data = {
        user_id: userId,
        favourite
      }
      await Api.setUserFavourite(data);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }

  async function onChangeBlockUser(userId, blocked) {
    try {
      setLoading(true);
      const data = {
        user_id: userId,
        blocked
      }
      await Api.blockUser(data);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }




  function getUserOptions(data) {
    return (
      data &&
      data.map((user) => {
        const fullName = `${user.first_name} ${user.last_name}`;
        return { label: fullName, value: fullName };
      })
    );
  }
  function handleSearchUser(value) {
    // debugger
    let filteredData = [];
    filteredData = users.filter(
      (user) =>
        user &&
        `${user.first_name} ${user.last_name}`
          .toLowerCase()
          .includes(value.toLowerCase())
    );

    filteredData = getUserOptions(filteredData);
    // console.log('value is',value)

    // setUserOptions([...filteredData]);
    // setSelectedOption(value)
  }

  function handleSelectUser(value) {
    // console.log('handleSelectUser',value)
    // let user = _.find(users,user=>user.user_id == value)
    // console.log('user 2is',user)
    // if(user){
    //   setTableData([user])
    //   let fullName = `${user.first_name} ${user.last_name}`;
    //   setSelectedOption(fullName)
    // }else{
    //   setTableData([])
    //   setSelectedOption('')
    // }
  }

  // const options =  usersOptions.map((user) => {
  //   console.log('user are::',user)
  //   return (
  //   <Option key={user.value.user_id} value={JSON.stringify(user.value)}>
  //     {`${user.value.first_name} ${user.value.last_name}`}
  //   </Option>)
  // })

  const tableColumns = useMemo(() => getColumns(onChangeFavouriteCustomer,onChangeBlockUser), []);

  return (
    <LoadingIndicator isLoading={isLoading}>
      <PageContent headerTitle='לקוחות'>
        <Row gutter={[10, 15]}>
          <Col span={4}>
            <AutoComplete
              dropdownMatchSelectWidth={252}
              // onChange={handleSearchUser}
              onSearch={handleSearchUser}
              options={usersOptions}
            >
              <Search placeholder='חפש משתמש' enterButton />
            </AutoComplete>
          </Col>
        </Row>
        <Table
          dataSource={tableData}
          pagination={{
            pageSize: 10,
            showTotal: total => ` סך הכל ${total} `,
          }}
          // // className={styles.table}
          scroll={{ x: 1200 }}
          columns={tableColumns}
          simple
          rowKey={(record) => record.id}
        />
      </PageContent>
    </LoadingIndicator>
  );
}

export default Customers;

import React from "react";

import { Dropdown, Button, Menu,Switch,Badge} from "antd";
import { BarsOutlined, DownOutlined } from "@ant-design/icons";

const getBadgeColor = (value) => value?'green':'red'

function getColumns(onChangeFavouriteCustomer,onChangeBlockCustomer) {
  const columns = [
    {
      title: "שם פרטי",
      dataIndex: "first_name",
    },
    {
      title: "שם משפחה",
      dataIndex: "last_name",
    },
    {
      title: "טלפון",
      dataIndex: "phone",
    },
    {
      title: "סטטוס הרשמה",
      dataIndex: "active",
      render: (active) => <Badge color={active?'green':'red'} text={active?'סיים הרשמה':'לא סיים'} />,

    },

    {
      title: "לקוח מועדף",
      dataIndex: "favourite",
      render:(favourite,record)=><Switch defaultChecked={favourite} onChange={(e)=>{onChangeFavouriteCustomer(record.user_id,e)}} />,

    },

    {
      title: "סטטוס",
      dataIndex: "blocked",
      render:(blocked,record)=><Switch defaultChecked={!blocked} onChange={(e)=>{onChangeBlockCustomer(record.user_id,e)}}  />,

    },

    {
      key: "operation",
      render: (text, record) => {
        const menu = [
          { key: "1", name: "עדכן" },
          { key: "2", name: "מחק" },
        ].map((item) => <Menu.Item key={item.key}>{item.name}</Menu.Item>);
        return (
          <Dropdown trigger={['click']} overlay={<Menu onClick={() => {}}>{menu}</Menu>}>
            <Button style={{ border: "none" }}>
              <BarsOutlined style={{ marginRight: 2 }} />
              <DownOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  return columns;
}

export default getColumns
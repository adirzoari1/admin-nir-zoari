import styled from "styled-components";
import { Badge as BadgeTag } from "antd";
import { Calendar } from 'react-big-calendar'

const Badge = styled(BadgeTag)`
.ant-badge-count{
    background-color: ${(props) => (props.active? "#52c41a" : "#ff4d4f")};
    width:80px;
}
 
`;



const BookingCalendar = styled(Calendar)`
  .rbc-month-header .rbc-header:last-child {
    border-left: none;
  }

  .rbc-row-bg .rbc-day-bg:last-child {
    border-left: none;
  }

  .rbc-header + .rbc-header {
    border-color: #eee;
  }
  .rbc-header {
    border-color: #eee;
  }
  .rbc-day-bg + .rbc-day-bg {
    border-color: #eee;
  }

  .rbc-agenda-table {
    .rbc-header + .rbc-header {
      border-right: 1px solid #eee;
      border-left: none;
    }
  }

  .rbc-agenda-view table.rbc-agenda-table tbody > tr > td + td {
    border-right: 1px solid #eee;
    border-left: none;
  }

  .rbc-agenda-view table.rbc-agenda-table {
    /* direction: ltr; */
  }

  .rbc-current-time-indicator {
    display: none;
    background-color: #ff3860 !important;
    border-color: #ff3860 !important;
    background-image: linear-gradient(141deg, #ff0561 0%, #ff3860 71%, #ff5257 100%) !important;
  }

  .rbc-event-content {
    text-align: right;
  }

  .rbc-event {
    background-color: #ff3860 !important;
    border-color: #ff3860 !important;
    background-image: linear-gradient(141deg, #ff0561 0%, #ff3860 71%, #ff5257 100%) !important;
    outline: 0 !important;
  }

  .rbc-agenda-view table.rbc-agenda-table thead > tr > th {
    text-align: right;
  }
  .rbc-day-bg + .rbc-day-bg {
    border-right: 1px solid #eee;
  }
  .rbc-month-row + .rbc-month-row {
    border-color: 1px solid #eee;
  }
  .rbc-month-view {
    min-height: 70vh;
    border: none;
  }
  .rbc-time-view {
    border: none;
  }
  .rbc-time-view-resources .rbc-time-gutter,
  .rbc-time-view-resources .rbc-time-header-gutter {
    border-color: #eee;
  }
  .rbc-time-header-content {
    border-color: #eee;
  }
  .rbc-time-content {
    border-color: #eee;
  }
  .rbc-off-range-bg {
    background: repeating-linear-gradient(-45deg, rgb(255, 255, 255), rgb(255, 255, 255) 3px, rgb(235, 235, 235) 3px, rgb(235, 235, 235) 4px);
  }

  .rbc-timeslot-group {
    border-color: #eee;
  }

  .rbc-time-content > * + * > * {
    /* border-right: 1px solid #eee;
    border-left: none; */
    border: none;
  }

  .rbc-agenda-view table.rbc-agenda-table tbody > tr > td + td {
    border-color: #eee;
  }

  .rbc-agenda-view table.rbc-agenda-table tbody > tr + tr {
    border-color: #eee;
  }

  .rbc-agenda-view table.rbc-agenda-table {
    border: none;
  }

  .rbc-time-content > * > * {
    border-left: 1px solid #eee;
  }

  .rbc-today {
    background: rgba(255, 56, 96, 0.2);
  }

  .rbc-toolbar {
    padding: 1em 0;
    button {
      background: transparent !important;
      border-radius: 0 !important;
      box-shadow: none !important;
      outline: 0 !important;
      border: none !important;
      cursor: pointer !important;
      font-size: 13px;
      &.rbc-active {
        background-color: #ff3860 !important;
        border-color: #ff3860 !important;
        background-image: -webkit-linear-gradient(309deg, #ff0561 0%, #ff3860 71%, #ff5257 100%) !important;
        background-image: linear-gradient(141deg, #ff0561 0%, #ff3860 71%, #ff5257 100%) !important;
        color: #fff !important;
        border-radius: 5px !important;
        box-shadow: 0 5px 15px rgba(255, 56, 96, 0.5) !important;
      }
    }
  }
`

export {
    BookingCalendar
}

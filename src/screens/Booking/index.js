import React, { useEffect, useState } from 'react';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import {  Layout,Button, Divider, Row } from 'antd';
import {  momentLocalizer } from 'react-big-calendar';
import { PlusOutlined,RedoOutlined } from '@ant-design/icons';

import _ from 'lodash';
import moment from 'moment';
import { LoadingIndicator, PageContent, NewBookModal, BookEventModal } from '../../components';
import { BOOKING_COLORS } from '../../utils/colors';
import { BookingCalendar } from './style';
import Api from '../../utils/api';

const localizer = momentLocalizer(moment);

const calendarMessages = { next: 'הבא', previous: 'הקודם', today: 'היום',week:'שבועי', day:'יומי',month:'חודשי' };
function Booking() {
  const [events, setEvents] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [bookModalVisible,setBookModalVisible] = useState(false)
  const [bookEventModalVisible, setBookEventModalVisible] = useState(false)
  const [bookEventModalData, setBookEventModalData] = useState({})


  useEffect(() => {
    getBookingsByDate(new Date());
  }, []);


  async function getBookingsByDate(date) {
    setLoading(true);
    const startDateMonth = moment(date).startOf('month').format('YYYY-MM-DD');
    const endDateMonth = moment(date).endOf('month').format('YYYY-MM-DD');
    const data = {
      start_date: startDateMonth,
      end_date: endDateMonth,
      barber_id: 1, // TODO: to change it - get by user login auth
    };

    const promises = [
      Api.getAllAppointmentsOfBarberByRangeDates(data),
      Api.getAllVacationsOfBarberByRange(data),
    ];

    try {
      let [appointments, vacations] = await Promise.all(promises);
      appointments = mappedAppointments(
        _.get(appointments, 'data.appointments', [])
      );
      vacations = mappedVacations(
        _.get(vacations, 'payload.data.schedules', [])
      );
      let events = _.union(appointments, vacations);
      setEvents(events);

      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }

  function mappedAppointments(appointments) {
    return appointments.map((a) => {
      let endTime = moment(a.time, 'HH:mm')
        .add(a.time_slot, 'minutes')
        .format('HH:mm');
      let title;
      if (a.note) {
        title = `תור ל${a.note} ל${a.service_type} - נרשם תחתי`;
      } else {
        title = `תור ל${a.first_name} ${a.last_name} ל${a.service_type} (${a.phone}) לנותן שירות ${a.barber_first_name} ${a.barber_last_name}`;
      }
      return {
        start: moment(`${a.date} ${a.time}`).toDate(),
        end: moment(`${a.date} ${endTime}`).toDate(),
        title,
        color: a.user_arrive ? '#33313b' : BOOKING_COLORS[a.service_id - 1],
        type: 'regular',
        data: a,
      };
    });
  }

  function mappedVacations(vacations) {
    return vacations.map((a) => {
      let title = `חופש לנותן שירות ${a.first_name} ${a.last_name}`;
      return {
        start: moment(a.date).toDate(),
        end: moment(a.date).toDate(),
        title,
        color: BOOKING_COLORS[2],
        allDay: true,
        type: 'special',
        special_schedule_id: a.special_schedule_id,
      };
    });
  }

  function onSelectEvent(e) {
    if(e.type == 'regular'){
      setBookEventModalData(e)
      setBookEventModalVisible(true)
    }
  
  }

  function onHandleSelectSlot(e) {
    console.log('onHandleSelectSlot e',e)

  }

  let date1 = new Date();
  let date2 = new Date(2100, 10, 10, 23, 50, 0);
  date1.setHours(6, 0, 0);
  return (
    <LoadingIndicator isLoading={isLoading}>
      <PageContent headerTitle='תורים'>
        <>
        <Row justify="space-between">
            <Button type='primary' onClick={()=>{setBookModalVisible(true)}} icon={<PlusOutlined />}>
                הוספת תור
            </Button>

            <Button type="dashed" onClick={()=>{getBookingsByDate(new Date())}} icon={<RedoOutlined />}>
                רענן תורים
            </Button>
            </Row>
          <Divider/>
          <BookingCalendar
            min={date1}
            max={date2}
            onNavigate={(e) => {
              getBookingsByDate(moment(e).format('YYYY-MM-DD'));
            }}
            localizer={localizer}
            events={events}
            defaultDate={new Date()}
            defaultView='month'
            selectable={true}
            onSelectEvent={onSelectEvent}
            onSelectSlot={onHandleSelectSlot}
            views={['month', 'day', 'week']}
            rtl={true}
            startAccessor='start'
            endAccessor='end'
            style={{ height: '100vh' }}
            eventPropGetter={(event) => ({
              style: {
                backgroundColor: event.color,
                textAlign: 'center',
              },
            })}
            messages={calendarMessages}
            timeslots={1}
            step={10}
            components={{
             
              day: {
                dateHeader: ({ date, localizer }) => `${localizer.format(date, 'dddd')}`
              }
            }}
          />
          <NewBookModal 
          visible={bookModalVisible}
          onCloseModal={()=>{
            getBookingsByDate(new Date())
            setBookModalVisible(false)}}
          
          />

          <BookEventModal 
          visible={bookEventModalVisible}
          onCloseModal={()=>{
            setBookEventModalVisible(false)
          }}
            bookEventData={bookEventModalData}
            getBookingsByDate={getBookingsByDate}
          />
          
       
        </>
      </PageContent>
    </LoadingIndicator>
  );
}

export default Booking;

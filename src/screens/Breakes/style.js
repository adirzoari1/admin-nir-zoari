import styled from "styled-components";
import { Badge as BadgeTag, Form } from "antd";

const Badge = styled(BadgeTag)`
.ant-badge-count{
    background-color: ${(props) => (props.active? "#52c41a" : "#ff4d4f")};
    width:80px;
}
 
`;

const FormStyle = styled(Form)`
    margin: 0 auto;
`

export {
    Badge,
    FormStyle
}
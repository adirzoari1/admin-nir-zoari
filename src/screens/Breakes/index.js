import React, { useEffect, useState, useMemo } from 'react';
import { Layout, Typography, Form, Input, Button, Space, Select } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import _ from 'lodash';
import { LoadingIndicator, PageContent } from '../../components';
import Api from '../../utils/api';
import { formLayout, tailFormItemLayout } from '../../utils/formLayoutUtils';
import { FormStyle } from './style';
import moment from 'moment';
import HourSlider from '../../components/HourSlider';
import 'moment/locale/he';

const { Content } = Layout;
const { Text, Title } = Typography;

function Breakes() {
  const [form] = Form.useForm();

  const [isLoading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const barberId = 1;

  useEffect(() => {
    getScheduleByBarberId(barberId);
  }, []);

  function timeToDecimal(t) {
    var arr = t.split(':');
    var dec = parseInt((arr[1] / 6) * 10, 10);

    return parseFloat(parseInt(arr[0], 10) + '.' + (dec < 10 ? '0' : '') + dec);
  }
  async function getScheduleByBarberId(barberId) {
    try {
      setLoading(true);
      let res = await Api.getBreaksByBarber(barberId);
      res = _.get(res, 'data.breaks', []).map((s) => ({
        ...s,
        dayIndex: s.day,
      }));
      setData([...res]);
      form.setFieldsValue({
        breaks: res,
      });
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }
  const onFinish = async () => {
    try {
      setLoading(true);
      let values = await form.validateFields();
      values = _.map(values.breaks, (v) => ({
        barber_id: 1, // TODO: to change to custom from auth context api,
        day: v.dayIndex,
        start_break_time: v.start_shift,
        end_break_time: v.end_shift,
      }));

      const data = {
        breaks: JSON.stringify(values),
      };
      const res = await Api.addBreaks(data);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  function updateScheduleData(schedule) {
    const { scheduleId, startShift, endShift } = schedule;
    const tempData = _.cloneDeep(data);
    const index = _.findIndex(tempData, { schedule_id: scheduleId });
    tempData.splice(index, 1, {
      ...tempData[index],
      start_shift: startShift,
      end_shift: endShift,
    });
    setData([...tempData]);
  }

  const SelectDay = useMemo(() => {
    return (
      <Select showSearch style={{ width: 200 }}>
        {_.range(1, 8).map((d, i) => {
          return (
            <Select.Option value={d} key={d}>
              {moment(d - 1, 'e').format('dddd')}
            </Select.Option>
          );
        })}
      </Select>
    );
  }, []);


 
  const SelectTime = useMemo(() => {
    return (
      <Select showSearch style={{ width: 200 }}>
        {Array.apply(null, Array(180)).map((d, i) => {
          let time = moment('06:00', 'HH:mm A')
            .add(i * 5, 'm')
            .format('HH:mm');
          return (
            <Select.Option value={time} key={i}>
              {time}
            </Select.Option>
          );
        })}
      </Select>
    );
  }, []);

  return (
    <LoadingIndicator isLoading={isLoading}>
      <PageContent headerTitle='הפסקות'>
        <FormStyle
          name='dynamic_form_nest_item'
          form={form}
          onFinish={onFinish}
          autoComplete='off'
          initialValues={{
            breaks: data,
          }}
          {...formLayout}
          labelCol={8}
        >
          <Form.List name='breaks'>
            {(fields, { add, remove }) => {
                return (
                <div>
                  {fields.map((field) => (
                    <Space
                      key={field.key}
                      style={{ display: 'flex', marginBottom: 8 }}
                      align='center'
                    >
                      <Form.Item
                        label='יום'
                        {...field}
                        name={[field.name, 'dayIndex']}
                        fieldKey={[field.fieldKey, 'dayIndex']}
                        rules={[{ required: true, message: 'שדה חובה' }]}
                      >
                        {SelectDay}
                      </Form.Item>

                      <Form.Item
                        label='שעת התחלה'
                        {...field}
                        name={[field.name, 'start_shift']}
                        fieldKey={[field.fieldKey, 'start_shift']}
                        rules={[{ required: true, message: 'שדה חובה' }]}
                      >
                        {SelectTime}
                      </Form.Item>
                      <Form.Item
                        label='שעת סיום'
                        {...field}
                        name={[field.name, 'end_shift']}
                        fieldKey={[field.fieldKey, 'end_shift']}
                        rules={[{ required: true, message: 'שדה חובה' }]}
                      >
                        {SelectTime}
                      </Form.Item>

                      <MinusCircleOutlined
                        style={{ fontSize: 20 }}
                        onClick={() => {
                          remove(field.name);
                        }}
                      />
                    </Space>
                  ))}

                  <Form.Item>
                    <Button
                      type='dashed'
                      onClick={() => {
                        add();
                      }}
                      block
                    >
                      <PlusOutlined />
                      הוספת הפסקה
                    </Button>
                  </Form.Item>
                </div>
              );
            }}
          </Form.List>

          <Form.Item>
            <Button type='primary' htmlType='submit'>
              שמור
            </Button>
          </Form.Item>
        </FormStyle>
      </PageContent>
    </LoadingIndicator>
  );
}

export default Breakes;

import styled from 'styled-components';
import { Badge as BadgeTag } from 'antd';
import { CheckCircleOutlined } from '@ant-design/icons';

const Badge = styled(BadgeTag)`
  .ant-badge-count {
    background-color: ${(props) => (props.active ? '#52c41a' : '#ff4d4f')};
    width: 80px;
  }
`;

const CheckCircleOutlinedIcon = styled(CheckCircleOutlined)`
  :active{
    color: #ff3860 ;
  }
`;
export { Badge, CheckCircleOutlinedIcon };

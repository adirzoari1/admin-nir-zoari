import React, { useEffect, useState, useMemo } from 'react';
import { Form, Button, Row, Col, notification } from 'antd';
import _ from 'lodash';
import { LoadingIndicator, PageContent } from '../../components';
import Api from '../../utils/api';
import { formLayout, tailFormItemLayout } from '../../utils/formLayoutUtils';
import moment from 'moment';
import HourSlider from '../../components/HourSlider';
import { CheckCircleOutlinedIcon } from './style';
import 'moment/locale/he';

function Schedule() {
  const [form] = Form.useForm();

  const [isLoading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const barberId = 1;

  useEffect(() => {
    getScheduleByBarberId(barberId);
  }, []);

  function timeToDecimal(t) {
    var arr = t.split(':');
    var dec = parseInt((arr[1] / 6) * 10, 10);

    return parseFloat(parseInt(arr[0], 10) + '.' + (dec < 10 ? '0' : '') + dec);
  }
  async function getScheduleByBarberId(barberId) {
    try {
      setLoading(true);
      let res = await Api.getScheduleByBarber(barberId);
      res = _.get(res, 'data.schedules', []).map((s) => ({
        ...s,
        day_name: moment(s.day - 1, 'e').format('dddd'),
        start_shift: timeToDecimal(s.start_shift),
        end_shift: timeToDecimal(s.end_shift),
      }));
      res = setData([...res]);
      form.setFieldsValue({
        ...res,
      });
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }

  async function updateScheduleById(schedule) {
    console.log('data is', data);
    console.log('scheduleId', schedule);
    try {
      setLoading(true);

      const scheduleData = {
        schedule_id: schedule.schedule_id,
        barber_id: schedule.barber_id,
        day: schedule.day,
        start_shift: moment()
          .clone()
          .set({ hour: 0, minutes: 0 })
          .add(schedule.start_shift, 'hours')
          .format('HH:mm'),
        end_shift: moment()
          .clone()
          .set({ hour: 0, minutes: 0 })

          .add(schedule.end_shift, 'hours')
          .format('HH:mm'),
      };

      await Api.updateSchedule(scheduleData);
      setLoading(false);
      notification.success({
        message:'הצלחה',
        description: 'העידכון בוצע בהצלחה'
      })
    } catch (e) {
      setLoading(false);
    }
  }

  function updateScheduleData(schedule) {
    const { scheduleId, startShift, endShift } = schedule;
    const tempData = _.cloneDeep(data);
    const index = _.findIndex(tempData, { schedule_id: scheduleId });
    tempData.splice(index, 1, {
      ...tempData[index],
      start_shift: startShift,
      end_shift: endShift,
    });
    setData([...tempData]);
  }

  return (
    <LoadingIndicator isLoading={isLoading}>
      <PageContent headerTitle='שעות פעילות'>
        <Form
          {...formLayout}
          form={form}
          name='form_schedule'
          scrollToFirstError
        >
          {_.map(data, (s, i) => (
            <Form.Item
              name={s.schedule_id}
              getValueFromEvent={updateScheduleData}
              key={i}
            >
              <Row justify='space-around' align='middle'>
                <Col span={18}>
                  <HourSlider
                    index={s.schedule_id}
                    active={true}
                    start_shift={s.start_shift}
                    end_shift={s.end_shift}
                    day={s.day}
                    onChecked={() => {}}
                    day={s.day_name}
                    onChange={updateScheduleData}
                  />
                </Col>
                <Col span={2} push={2}>
                  <CheckCircleOutlinedIcon
                    style={{ fontSize: 20 }}
                    onClick={() => {
                      updateScheduleById(s);
                    }}
                  />
                </Col>
              </Row>
              {/* <Row  gutter={[16,16]} justify='space-around' align='middle'>
                  <Col span={4}>
                    <Text strong>{s.dayName}</Text>
                  </Col>
                  <Col span={8}>
                    <Title level={5} type="secondary">שעת התחלה</Title>
                    <TimePicker
                      defaultValue={moment(s.start_shift, 'HH:mm')}
                      format='HH:mm '
                    />
                  </Col>
                  <Col span={8}>
                    <Title level={5} type="secondary">שעת סיום</Title>
                    <TimePicker
                      defaultValue={moment(s.end_shift, 'HH:mm')}
                      format='HH:mm '
                    />
                  </Col>
                  <Col span={4}>
                  <Button type="primary">עדכן</Button>
                  </Col>
                </Row> */}
            </Form.Item>
          ))}
        </Form>
      </PageContent>
    </LoadingIndicator>
  );
}

export default Schedule;

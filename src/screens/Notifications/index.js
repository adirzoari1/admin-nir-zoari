import React, { useEffect, useState, useMemo } from 'react';
import { Layout, Table, PageHeader, Button } from 'antd';
import _ from 'lodash';
import getColumns from './columns';
import {
  NewNotificationModal,
  LoadingIndicator,
  PageContent,
} from '../../components';
import { PlusOutlined } from '@ant-design/icons';
import Api from '../../utils/api';


const { Content } = Layout;
const DEFAULT_QUERY = {
  LIMIT: 10,
  OFFSET: 0,
};

function Notifications() {
  const [notifications, setNotifications] = useState([]);
  const [notificationModalVisible, setNotificationModalVisible] = useState(
    false
  );
  const [isLoading, setLoading] = useState(false);

  const BARBER_ID = 1; // TODO: to change it to get from auth store
  useEffect(() => {
    getNotificationsByBarber(DEFAULT_QUERY.LIMIT, DEFAULT_QUERY.OFFSET);
  }, []);

  async function getNotificationsByBarber(
    limit = DEFAULT_QUERY.LIMIT,
    offset = DEFAULT_QUERY.OFFSET
  ) {
    try {
      setLoading(true);
      let res = await Api.getNotificationsByBarber(BARBER_ID, limit, offset);
      res = _.get(res, 'data.notifications', []);
      setNotifications([...res]);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }

  function onSelectMenu(key, record) {
    switch (key) {
      case 'delete_notification':
        break;

      default:
        break;
    }
  }
  const tableColumns = useMemo(() => getColumns(onSelectMenu), [onSelectMenu]);

  return (
    <LoadingIndicator isLoading={isLoading}>
      <PageContent headerTitle='הודעות'>
        <Button
          type='primary'
          icon={<PlusOutlined />}
          onClick={() => {
            setNotificationModalVisible(true);
          }}
          style={{ marginBottom: '20px' }}
        >
          הוספת הודעה
        </Button>
        <Table
          dataSource={notifications}
          pagination={{
            pageSize: 10,
            // showTotal: total => i18n.t`Total ${total} Items`,
          }}
          columns={tableColumns}
          simple
          rowKey='notification_id'
          scroll={{ x: 1200 }}
        />
        <NewNotificationModal
          visible={notificationModalVisible}
          onSubmit={async () => {
            await getNotificationsByBarber();
            setNotificationModalVisible(false);
          }}
          onCloseModal={() => {
            setNotificationModalVisible(false);
          }}
        />
      </PageContent>
    </LoadingIndicator>
  );
}

export default Notifications;

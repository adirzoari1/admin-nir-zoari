import React from "react";
import moment from "moment";
import { Dropdown, Button, Menu, Switch, Avatar, Badge } from "antd";
import { BarsOutlined, DownOutlined } from "@ant-design/icons";
const getBadgeColor = (value) => (value ? "green" : "red");

const SHOW_DELETE_NOTIFICATIONS = ["app_notification"];
 const NOTIFICATIONS_TYPES = [
  {value:'app_notification',text:'הודעת אפליקציה'},
  {value:'push_notification',text:'הודעת פוש נוטיפיקישין'},
  {value:'sms',text:'הודעת SMS'}

]

function getBadgeNotification(type) {
  switch (type) {
    case "app_notification":
      return { color: "cyan", text: "הודעות אפליקציה" };
    case "push_notification":
      return { color: "yellow", text: "הודעת פוש נוטיפיקישין" };
    default: {
      return { color: "cyan", text: "הודעות אפליקציה" };
    }
  }
}

function renderDate(date, type) {
  switch (type) {
    case "app_notification":
      return moment(date).format("YYYY-MM-DD");
    default:
      return "";
  }
}

function showDropDown(record){
  return SHOW_DELETE_NOTIFICATIONS.indexOf(record.type)>-1 && moment().isBetween(moment(record.start_date,'YYYY-MM-DD'),moment(record.end_date,'YYYY-MM-DD'))
}


function getColumns(onSelectMenu) {
  const columns = [
    {
      title: "מזהה הודעה",
      dataIndex: "notification_id",
      responsive: ['md'],
    },

    {
      title: "סוג הודעה",
      dataIndex: "type",
      responsive: ['md'],
      filters: NOTIFICATIONS_TYPES,
      filterMultiple: true,
      onFilter: (value, record) => record.type.indexOf(value) === 0,
      render: (type) => <Badge {...getBadgeNotification(type)} />,
      
    },

    {
      title: "כותרת הודעה",
      dataIndex: "title",
      responsive: ['md'],

    },

    {
      title: "תוכן הודעה",
      dataIndex: "description",
      responsive: ['md'],

    },

    {
      title: "תאריך התחלה",
      dataIndex: "start_date",
      render: (start_date, record) =>moment(start_date).format("YYYY-MM-DD"),
      responsive: ['md'],
    },
    {
      title: "תאריך סיום",
      dataIndex: "end_date",
      render: (end_date, record) => moment(end_date).format("YYYY-MM-DD"),
      responsive: ['md'],
    },
    {
      key: "operation",
      render: (text, record) => {
        const menu = [
        { key: "delete_notification", name: "מחיקת הודעה" }].map((item) => (
          <Menu.Item key={item.key}>{item.name}</Menu.Item>
        ));
        return (
           showDropDown(record) &&<Dropdown
            overlay={
              <Menu
                onClick={(e) => {
                  onSelectMenu(e.key, record);
                }}
              >
                {menu}
              </Menu>
            }
          >
            <Button style={{ border: "none" }}>
              <BarsOutlined style={{ marginRight: 2 }} />
              <DownOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  return columns;
}

export default getColumns;

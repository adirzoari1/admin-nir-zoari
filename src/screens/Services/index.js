import React, { useEffect, useState, useMemo } from 'react';
import { Layout, Table, PageHeader, Button } from 'antd';
import _ from 'lodash';
import getColumns from './columns';
import {
  UpsertServiceModal,
  LoadingIndicator,
  PageContent,
} from '../../components';
import { PlusOutlined } from '@ant-design/icons';
import Api from '../../utils/api';

const { Content } = Layout;

function Services() {
  const [services, setServices] = useState([]);
  const [upsertModalVisible, setUpsertServiceVisible] = useState(false);
  const [selectedRecord, setSelectedRecord] = useState({});
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    getServices();
  }, []);
  function onShowUpsertModal(record) {
    setSelectedRecord({ ...record });
    setUpsertServiceVisible(true);
  }

  

  function onSelectMenu(key, record) {
    switch (key) {
      case 'edit_service':
        onShowUpsertModal(record);
        break;

      default:
        break;
    }
  }
  const tableColumns = useMemo(
    () => getColumns(onSelectMenu, onShowUpsertModal),
    []
  );

  async function getServices(limit, offset) {
    try {
      setLoading(true);
      let res = await Api.getAllBarbersServices();
      res = _.get(res, 'data.services', []);
      console.log('res is',res)
      setServices([...res]);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }

  return (
    <LoadingIndicator isLoading={isLoading}>
      <PageContent headerTitle='שירותים'>
        <Button
          type='primary'
          icon={<PlusOutlined />}
          onClick={() => {
            onShowUpsertModal({});
          }}
          style={{ marginBottom: '20px' }}
        >
          הוספת שירות
        </Button>
        <Table
          dataSource={services}
          pagination={{
            pageSize: 10,

            // showTotal: total => i18n.t`Total ${total} Items`,
          }}
          scroll={{ x: 1200 }}
          columns={tableColumns}
          simple
          rowKey='service_id'
          
        />
        <UpsertServiceModal
          visible={upsertModalVisible}
          record={selectedRecord}
          onSubmit={async() => {
            await getServices()
            setSelectedRecord({});
            setUpsertServiceVisible(false);
          }}
          onCloseModal={()=>{
            setUpsertServiceVisible(false);

          }}
        />
      </PageContent>
    </LoadingIndicator>
  );
}

export default Services;

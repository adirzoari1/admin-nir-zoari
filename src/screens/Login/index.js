import React, { useState, useContext,useEffect } from 'react';
import { Wrapper, Container, Content } from './style';
import { LoadingIndicator } from '../../components';
import { Form, Input,Button,notification } from 'antd';
import { formLayout,tailFormItemLayout } from '../../utils/formLayoutUtils';
import Api from '../../utils/api';
import { AuthContext } from '../../routes/context';
import _ from 'lodash'
import { useHistory } from 'react-router';
function Login() {
  const authContext = useContext(AuthContext);
  const [form] = Form.useForm();
  const [isLoading, setLoading] = useState(false);
  const [showCodeInput, setShowCodeInput] = useState(false);
  const history = useHistory()


  useEffect(()=>{
    if(authContext.isAuthenticated){
      history.push('/app')
    }
  },[authContext])

  const onSubmit = async()=>{
    try{
      let values = await form.validateFields()
      if(showCodeInput){
        handleVerifyCode(values)
      }else{
        handleVerifyPhone(values.phone)
      }
    }catch(err){
  
    }
  }

  const handleVerifyPhone = async(phone)=>{
    try{
      setLoading(true)
      await Api.login(phone)
      setLoading(false)
      setShowCodeInput(true)

    }catch(err){
      setLoading(false)
      notification.error({
        message: 'שגיאה',
        description: err.message,
      })
    }
  }

  const handleVerifyCode = async({phone,code})=>{
    try{
      setLoading(true)
      const data = {
        phone,
        sms_code: code
      }
      const res = await Api.verifyPhoneNumber(data)
      authContext.setUserData(_.get(res,'data',{}))
      setLoading(false)
      setShowCodeInput(true)
      history.push('/app')
    }catch(err){
      setLoading(false)
      notification.error({
        message: 'שגיאה',
        description: err.message,
      })
    }
  }

  return (
    <LoadingIndicator isLoading={isLoading}>
      <Wrapper>
        <Container>
          <Content>
            <Form
              {...formLayout}
              form={form}
              name='form_login'
              scrollToFirstError
              onFinish={onSubmit}
            >
              <Form.Item
                name='phone'
                label='טלפון'
                rules={[
                  {
                    validator: (rule, value, callback) => {
                      if (value && value.length != 10) {
                        callback('מספר טלפון לא תקין');
                      }
                      callback();
                    },
                  
                  },
                  {
                    required:true,
                    message:'שדה חובה'
                  }
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item noStyle shouldUpdate>
                {({ getFieldValue }) =>
                  showCodeInput ? (
                    <Form.Item
                      name='code'
                      label='קוד'
                      rules={[
                        {
                          required: true,
                          message: 'שדה חובה',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                  ) : null
                }
              </Form.Item>

              <Form.Item {...tailFormItemLayout}>
            <Button type='primary' htmlType='submit'>
                  {showCodeInput? 'אמת קוד': 'התחברות'}
            </Button>
          </Form.Item>
            </Form>
          </Content>
        </Container>
      </Wrapper>
    </LoadingIndicator>
  );
}

export default Login;

import styled from "styled-components";

const Wrapper = styled.div`
   min-height: 100vh;
   align-items: stretch;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`
const Container = styled.div`
    align-items: center;
    display: flex;
    padding: 3rem 1.5rem;
    flex-grow: 1;
    flex-shrink: 0;
    margin: 0 auto;
    position: relative;
`

const Content = styled.div`
     flex-grow: 1;
    flex-shrink: 1;
    max-width: 1344px;
`

export {
    Wrapper,
    Container,
    Content
}
import React from "react";

import { Dropdown, Button, Menu,Switch, Badge} from "antd";
import { BarsOutlined, DownOutlined } from "@ant-design/icons";


function getColumns(onSelectMenu,onShowUpsertModal,onChangeServiceStatus) {
  const columns = [
    {
      title: "מזהה נותן שירות",
      dataIndex: "barber_id",
      width:'10%'
    },
    {
      title: "שם פרטי",
      dataIndex: "first_name",
    },
    {
      title: "שם משפחה",
      dataIndex: "last_name",
    },
    {
      title: "טלפון",
      dataIndex: "phone",
    },
    {
      title: "סטטוס",
      dataIndex: "active",
      render: (active) => <Badge color={active?'green':'red'} text={active?'פעיל':'לא פעיל'} />,

    },

   

    {
      key: "operation",
      render: (text, record) => {
        const menu = [
          { key: "edit_service", name: "עדכן" },
        ].map((item) => <Menu.Item key={item.key}>{item.name}</Menu.Item>);
        return (
          <Dropdown trigger={['click']} overlay={<Menu onClick={(e)=>{onSelectMenu(e.key,record)}}>{menu}</Menu>}>
            <Button style={{ border: "none" }}>
              <BarsOutlined style={{ marginRight: 2 }} />
              <DownOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  return columns;
}

export default getColumns
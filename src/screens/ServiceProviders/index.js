import React, { useEffect, useState, useMemo } from "react";
import { Layout, Table, PageHeader,Button } from "antd";
import _ from "lodash";
import getColumns from "./columns";
import { UpsertServiceModal,LoadingIndicator,PageContent } from "../../components";
import { PlusOutlined } from "@ant-design/icons";
import Api from "../../utils/api";

const { Content } = Layout;

function ServiceProviders() {
  const [servicesProviders,setServicesProviders] = useState([])
  const [upsertModalVisible, setUpsertServiceVisible] = useState(false);
  const [selectedRecord, setSelectedRecord] = useState({});
  const [isLoading, setLoading] = useState(false);


  useEffect(() => {
    getServicesProviders();
  }, []);

  function onShowUpsertModal(record) {
    setSelectedRecord({ ...record });
    setUpsertServiceVisible(true);
  }

  function onSelectMenu(key, record) {
    switch (key) {
      case "edit_service":
        onShowUpsertModal(record);
        break;

      default:
        break;
    }
  }
  const tableColumns = useMemo(
    () => getColumns(onSelectMenu, onShowUpsertModal),
    [onSelectMenu]
  );

  async function getServicesProviders(limit, offset) {
    try {
      setLoading(true);
      let res = await Api.getAllBarbers();
      res = _.get(res, 'data.users', []);
      setServicesProviders([...res]);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }

  return (
    <LoadingIndicator isLoading={isLoading}>

    <PageContent headerTitle='נותני שירות'>
        <Button  type="primary" icon={<PlusOutlined />} onClick={()=>{onShowUpsertModal({})}} style={{marginBottom:'20px'}}>
          הוספת נותן שירות
        </Button>
        <Table
          dataSource={servicesProviders}
          pagination={{
            pageSize: 10,

            // showTotal: total => i18n.t`Total ${total} Items`,
          }}
          scroll={{ x: 1200 }}
          columns={tableColumns}
          simple
          rowKey="service_id"
        />
        <UpsertServiceModal
          visible={upsertModalVisible}
          record={selectedRecord}
          onCloseModal={() => {
            setSelectedRecord({});
            setUpsertServiceVisible(false);
          }}
        />
    </PageContent>
    </LoadingIndicator>
  );
}

export default ServiceProviders;

import React, { Component } from "react";
import { Layout } from "antd";
import {Dashboard, Customers,Services,Notifications,ServiceProviders, BusinessGeneralDetails,Booking,Schedule,Breakes} from "..";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,

} from '@ant-design/icons';
import { Aside } from "../../components";
import { Switch, Route, Router } from "react-router-dom";

const { Content, Header } = Layout;

export default class App extends Component {
  state = {
    collapsed: false,
  };
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  
  render() {
    return (
      <Layout style={{ height:'100%'}}>
        <Aside collapsed={this.state.collapsed} />
        <Layout>
          <Header theme="light" style={{ backgroundColor: 'white' }} >
          {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: this.toggle,
            })}
          </Header>
          <Content style={{ margin: '24px 16px 0' }}>
            <Router history={this.props.history}>
              <Switch>
                <Route exact path="/app" component={Dashboard} />
                <Route exact path="/app/customers" component={Customers} />
                <Route exact path="/app/services" component={Services} />
                <Route exact path="/app/service-providers" component={ServiceProviders} />
                <Route exact path="/app/notifications" component={Notifications} />
                <Route exact path="/app/business-general-details" component={BusinessGeneralDetails} />
                <Route exact path="/app/booking" component={Booking} />
                <Route exact path="/app/schedule" component={Schedule} />
                <Route exact path="/app/breaks" component={Breakes} />

               </Switch>
            </Router>
          </Content>
        </Layout>
      </Layout>
    );
  }
}


import React, { useEffect, useState } from 'react';
import { Layout, PageHeader, Button, Form, Input, InputNumber } from 'antd';
import _ from 'lodash';
import {
  tailFormItemLayout,
  formItemLayout,
  formLayout,
} from '../../utils/formLayoutUtils';
import Api from '../../utils/api';
import { LoadingIndicator, PageContent } from '../../components';
const { Content } = Layout;

function BusinessGeneralDetails() {
  const [form] = Form.useForm();
  const [data, setData] = useState({});
  const [isLoading, setLoading] = useState(false);

  useEffect(()=>{
    getBussinessGeneralDetails()
  }, []);

  async function getBussinessGeneralDetails() {
    try {
      setLoading(true);
      const barberShopId = 1;
      let res = await Api.getBarbershop(barberShopId);
      res = _.get(res, 'data.Barbershop', {});
      setData({ ...res });
      form.setFieldsValue({
        ...res,
      });
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }



  return (
    <LoadingIndicator isLoading={isLoading}>
      <PageContent headerTitle='פרטי העסק'>
        <Form
          {...formLayout}
          form={form}
          name='form_bussiness'
          initialValues={{
            name: _.get(data, 'name', ''),

     
          }}
          scrollToFirstError
        >
          <Form.Item
            name='name'
            label='שם המספרה'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name='phone'
            label='טלפון'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name='longitude'
            label='מיקום - longitude'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name='latitude'
            label='מיקום - latitude'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name='facebook_link'
            label='פייסבוק'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name='instegram_link'
            label='אינסטגרם'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name='minutes_cancel_appointment'
            label='זמן ביטול תור'
            rules={[
              {
                required: true,
                message: 'שדה חובה',
              },
            ]}
          >
            <InputNumber />
          </Form.Item>

          <Form.Item {...tailFormItemLayout}>
            <Button type='primary' htmlType='submit'>
              עדכן
            </Button>
          </Form.Item>
        </Form>
      </PageContent>
    </LoadingIndicator>
  );
}

export default BusinessGeneralDetails;

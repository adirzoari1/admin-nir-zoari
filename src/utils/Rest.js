import config from "./config";
// import Logger from "./logger";
// import UserStore from "../stores/UserStore";
// import toastr from "../components/Toastr";
// import { bugsnagClient } from "./bugsnag";
// const loggger = new Logger({ batch_size: 10 });

class Rest {
  constructor() {
    this.API_URL = config[process.env.REACT_APP_ENV].API_URL;
    console.log('this API_URl',this.API_URL)
  }

  withQuery(url, params) {
    let query = Object.keys(params)
      .filter(k => params[k] !== undefined)
      .map(k => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
      .join("&");
    url += (url.indexOf("?") === -1 ? "?" : "&") + query;
    return url;
  }

  async cache(auth, update, path, method) {
    const cacheData = sessionStorage.getItem(path);
    if (cacheData && !update) {
      return JSON.parse(cacheData);
    }

    const data = await this.send(auth, path, method);
    sessionStorage.setItem(path, JSON.stringify(data));
    return data;
  }

  async download(auth, path, method, body, params, file) {
    const headers = {};
    // headers["Accept"] = "application/json";
    // if (auth && UserStore.token) {
    //   // headers["Authorization"] = "Bearer " + UserStore.token;
    //   headers["Authorization"] = UserStore.token;
    // }
    let url = `${this.API_URL}${path ? path : ""}`;
    if (params) {
      url = this.withQuery(url, params);
    }
    // window.open(url, "_blank");
    var link = document.createElement("a");
    link.setAttribute("href", url);
    // link.setAttribute("download", filename);
    link.click();
  }

  async send(auth, path, method, body, params, file) {
    const headers = {};
    headers["Accept"] = "application/json";

    if (!file) {
      headers["Content-Type"] = "application/json";
    }
    // if (auth && UserStore.token) { // TODO: to undo later
    //   // headers["Authorization"] = "Bearer " + UserStore.token;
    //   headers["Authorization"] = UserStore.token;
    // }
    let url = `${this.API_URL}${path ? path : ""}`;
    if (params) {
      url = this.withQuery(url, params);
    }
    try {
      let response = await fetch(url, {
        method: method,
        headers: headers,
        body: body != null && !file ? JSON.stringify(body) : file ? body : null
      });

      let resB;
      if (response.status !== 200) {
        try {
          resB = await response.json();
        } catch (err) {}
      
      }

      if (response.status === 401) {
        // return Promise.reject(response);
        // UserStore.logOut();
      }
      if (response.status === 400) {
        return Promise.reject(resB);
      }
      if (response.status === 404) {
        return Promise.reject(resB);
      }
      if (response.status === 503) {
        // toastr.warning("system is under maintenance");
        return Promise.reject(resB);
      }
      let responseJson = await response.json();
      return responseJson;
    } catch (error) {
      console.error(error);
    }
  }
}

const rest = new Rest();

export default rest;

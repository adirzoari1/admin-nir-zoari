import Rest from "./Rest";

class Api {
  constructor() {}


  // BARBER API
  // barber
  register = data => {
    return Rest.send(false, "/api/barbers/barber/register", "POST", data);
  };

  verifyPhoneNumber = data => {
    return Rest.send(
      false,
      "/api/barbers/barber/verifyPhoneNumber",
      "POST",
      data
    );
  };

  resendSMS = phone => {
    return Rest.send(false, `/api/barbers/barber/resendSMS/${phone}`, "GET");
  };

  login = phone => {
    return Rest.send(false, `/api/barbers/barber/login/${phone}`, "GET");
  };
  updateBarber(data) {
    return Rest.send(false, "/api/barbers/barber/updateBarber", "PATCH", data);
  }

  // appointment
  addAppointment = data => {
    return Rest.send(
      false,
      "/api/barbers/appointment/addAppointment",
      "POST",
      data
    );
  };

  cancelAppointment = appointment_id => {
    return Rest.send(
      false,
      `/api/barbers/appointment/deleteAppointmentById/${appointment_id}`,
      "DELETE"
    );
  };

  getAllMyAppointments = barber_id => {
    return Rest.send(
      false,
      `/api/barbers/appointment/getAllMyappointments/${barber_id}`,
      "GET"
    );
  };

  getAllMyFutureAppointments = query => {
    return Rest.send(
      false,
      `/api/barbers/appointment/getAllMyFutureAppointments`,
      "GET",
      null,
      query
    );
  };

  getAllAppointmentsOfBarberByRangeDates = data => {
    return Rest.send(
      false,
      `/api/barbers/appointment/getAllAppointmentsOfBarberByRangeDates`,
      "POST",
      data
    );
  };
  getAllAppointmentsByDateRange = data => {
    return Rest.send(
      false,
      `/api/barbers/appointment/getAllAppointmentsByDateRange`,
      "POST",
      data
    );
  };

  getAllMyAppointmentsByDate = (date, barber_id) => {
    return Rest.send(
      false,
      `/api/barbers/appointment/getAllMyAppointmentsByDate?barber_id=${barber_id}&&date=${date}`,
      "GET"
    );
  };

  getAllArrivedUserToAppointmentByDate = date => {
    return Rest.send(
      false,
      `/api/barbers/appointment/getAllArrivedUserToAppointmentByDate?date=${date}`,
      "GET"
    );
  };

  updateUserArrive = data => {
    return Rest.send(
      false,
      "/api/barbers/appointment/updateUserArrive",
      "PATCH",
      data
    );
  };

  //awaiting Appointments
  getAllMyAwaitingAppointmentsByDate = (date, barber_id) => {
    return Rest.send(
      false,
      `/api/barbers/awaitingAppointment/getAllMyAwaitingAppointmentsByDate?barber_id=${barber_id}&&date=${date}`,
      "GET"
    );
  };

  //awaiting Appointments
  getAllAwaitingAppointmentsByDate = date => {
    return Rest.send(
      false,
      `/api/barbers/awaitingAppointment/getAllAwaitingAppointmentsByDate?date=${date}`,
      "GET"
    );
  };

  // notification
  // appointment
  addNotification = data => {
    return Rest.send(
      false,
      "/api/barbers/notification/addNotification",
      "POST",
      data
    );
  };

  getNotificationsByBarber = (barber_id, limit, offset) => {
    return Rest.send(
      false,
      `/api/barbers/notification/getNotificationsByBarber?barber_id=${barber_id}&&limit=${limit}&&offset=${offset}`,
      "GET"
    );
  };

  getTotalNotificationsCountByBarber = barber_id => {
    return Rest.send(
      false,
      `/api/barbers/notification/getTotalNotificationsCountByBarber/${barber_id}`,
      "GET"
    );
  };

  getNotificationById = notification_id => {
    return Rest.send(
      false,
      `/api/barbers/notification/getNotificationById/${notification_id}`,
      "GET"
    );
  };
  sendGeneralPushNotification = data => {
    return Rest.send(
      false,
      "/api/barbers/notification/sendGeneralPushNotification",
      "POST",
      data
    );
  };

  // schedule

  addSchedule = data => {
    return Rest.send(false, "/api/barbers/schedule/addSchedule", "POST", data);
  };

  updateSchedule = data => {
    return Rest.send(
      false,
      "/api/barbers/schedule/updateSchedule",
      "PATCH",
      data
    );
  };

  getScheduleByBarber = barber_id => {
    return Rest.send(
      false,
      `/api/barbers/schedule/getScheduleByBarber/${barber_id}`,
      "GET"
    );
  };

  // specialSchedule

  addSpecialSchedule = data => {
    return Rest.send(
      false,
      "/api/barbers/specialSchedule/addSpecialSchedule",
      "POST",
      data
    );
  };

  deleteSpecialSchedule = special_schedule_id => {
    return Rest.send(
      false,
      `/api/barbers/specialSchedule/deleteSpecialScheduleById/${special_schedule_id}`,
      "DELETE"
    );
  };

  updateSpecialSchedule = data => {
    return Rest.send(
      false,
      "/api/barbers/specialSchedule/updateSpecialSchedule",
      "PATCH",
      data
    );
  };

  getAllVacationsOfBarberByRange = data => {
    return Rest.send(
      false,
      "/api/barbers/specialSchedule/getAllVacationsOfBarberByRange",
      "POST",
      data
    );
  };
  getAllVacationsByRange = data => {
    return Rest.send(
      false,
      "/api/barbers/specialSchedule/getAllVacationsByRange",
      "POST",
      data
    );
  };

  getSpecialScheduleByBarber = barber_id => {
    return Rest.send(
      false,
      `/api/barbers/specialSchedule/getSpecialScheduleByBarber/${barber_id}`,
      "GET"
    );
  };

  // breakes


  getBreaksByBarber = barber_id => {
    return Rest.send(
      false,
      `/api/barbers/break/getBreakByBarber/${barber_id}`,
      "GET"
    );
  };


  // service

  addService = data => {
    return Rest.send(false, "/api/barbers/service/addService", "POST", data);
  };

  deleteService = service_id => {
    return Rest.send(
      false,
      `/api/barbers/service/deleteServiceById/${service_id}`,
      "DELETE"
    );
  };

  updateService = data => {
    return Rest.send(
      false,
      "/api/barbers/service/updateService",
      "PATCH",
      data
    );
  };

  getServicesBybarber = barber_id => {
    return Rest.send(
      false,
      `/api/barbers/service/getServicesByBarber/${barber_id}`,
      "GET"
    );
  };

  getServiceById = service_id => {
    return Rest.send(
      false,
      `/api/barbers/service/getServiceById/${service_id}`,
      "GET"
    );
  };

  getAllBarbersServices = () => {
    return Rest.send(
      false,
      `/api/barbers/service/getAllBarbersServices`,
      "GET"
    );
  };

  //user
  getAllUsers = (limit, offset) => {
    return Rest.send(
      false,
      `/api/barbers/user/getAllUsers?limit=${limit}&&offset=${offset}`,
      "GET"
    );
  };

  getAllBarbers = () => {
    return Rest.send(false, `/api/barbers/barber/getBarbers`, "GET");
  };
  getTotalUsersCount = () => {
    return Rest.send(false, "/api/barbers/user/getTotalUsersCount", "GET");
  };

  blockUser = data => {
    return Rest.send(false, "/api/barbers/user/blockUser", "PATCH", data);
  };

  getBarberById = barber_id => {
    return Rest.send(
      false,
      `/api/barbers/barber/getBarberById/${barber_id}`,
      "GET"
    );
  };

  setUserFavourite = data => {
    return Rest.send(
      false,
      "/api/barbers/user/setUserFavourite",
      "PATCH",
      data
    );
  };


  createNewUser = data => {
    return Rest.send(
      false,
      "/api/barbers/user/createNewUser",
      "POST",
      data
    );
  };




  //barbershop
  getBarbershop = barbershop_id => {
    return Rest.send(
      false,
      `/api/barbers/barbershop/getBarbershopById/${barbershop_id}`,
      "GET"
    );
  };

  updateBarberShopDetails = data => {
    return Rest.send(
      false,
      `/api/barbers/barbershop/updateBarbershop`,
      "PATCH",
      data
    );
  };

  // schedule
  getAvailableScheduleOfBarber = query => {
    return Rest.send(
      false,
      "/api/barbers/schedule/getAvailableScheduleOfBarber",
      "GET",
      null,
      query
    );
  };

  // breaks


  addBreaks = data => {
    return Rest.send(
      false,
      "/api/barbers/break/addBreak",
      "POST",
      data
    );
  };


}

const api = new Api();

export default api;

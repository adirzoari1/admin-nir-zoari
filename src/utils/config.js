export default {
    local: {
      API_URL: "http://localhost:3014",
    },
    development: {
      // API_URL: "https://amirambenzakenapi.herokuapp.com",
      API_URL: "https://nir-zoari-api.herokuapp.com",

    },
    production: {
      API_URL: "https://amirambenzakenapi.herokuapp.com",
    }
  };
  